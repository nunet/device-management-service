// Code generated by MockGen. DO NOT EDIT.
// Source: actor/interface.go
//
// Generated by this command:
//
//	mockgen -destination=dms/node/mock_actor_test.go -source=actor/interface.go -package=node
//

// Package node is a generated GoMock package.
package node

import (
	context "context"
	reflect "reflect"
	time "time"

	actor "gitlab.com/nunet/device-management-service/actor"
	crypto "gitlab.com/nunet/device-management-service/lib/crypto"
	did "gitlab.com/nunet/device-management-service/lib/did"
	ucan "gitlab.com/nunet/device-management-service/lib/ucan"
	gomock "go.uber.org/mock/gomock"
)

// MockActor is a mock of Actor interface.
type MockActor struct {
	ctrl     *gomock.Controller
	recorder *MockActorMockRecorder
}

// MockActorMockRecorder is the mock recorder for MockActor.
type MockActorMockRecorder struct {
	mock *MockActor
}

// NewMockActor creates a new mock instance.
func NewMockActor(ctrl *gomock.Controller) *MockActor {
	mock := &MockActor{ctrl: ctrl}
	mock.recorder = &MockActorMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockActor) EXPECT() *MockActorMockRecorder {
	return m.recorder
}

// AddBehavior mocks base method.
func (m *MockActor) AddBehavior(behavior string, continuation actor.Behavior, opt ...actor.BehaviorOption) error {
	m.ctrl.T.Helper()
	varargs := []any{behavior, continuation}
	for _, a := range opt {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "AddBehavior", varargs...)
	ret0, _ := ret[0].(error)
	return ret0
}

// AddBehavior indicates an expected call of AddBehavior.
func (mr *MockActorMockRecorder) AddBehavior(behavior, continuation any, opt ...any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	varargs := append([]any{behavior, continuation}, opt...)
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "AddBehavior", reflect.TypeOf((*MockActor)(nil).AddBehavior), varargs...)
}

// Children mocks base method.
func (m *MockActor) Children() map[did.DID]actor.Handle {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Children")
	ret0, _ := ret[0].(map[did.DID]actor.Handle)
	return ret0
}

// Children indicates an expected call of Children.
func (mr *MockActorMockRecorder) Children() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Children", reflect.TypeOf((*MockActor)(nil).Children))
}

// Context mocks base method.
func (m *MockActor) Context() context.Context {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Context")
	ret0, _ := ret[0].(context.Context)
	return ret0
}

// Context indicates an expected call of Context.
func (mr *MockActorMockRecorder) Context() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Context", reflect.TypeOf((*MockActor)(nil).Context))
}

// CreateChild mocks base method.
func (m *MockActor) CreateChild(id string, super actor.Handle, opts ...actor.CreateChildOption) (actor.Actor, error) {
	m.ctrl.T.Helper()
	varargs := []any{id, super}
	for _, a := range opts {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "CreateChild", varargs...)
	ret0, _ := ret[0].(actor.Actor)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// CreateChild indicates an expected call of CreateChild.
func (mr *MockActorMockRecorder) CreateChild(id, super any, opts ...any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	varargs := append([]any{id, super}, opts...)
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateChild", reflect.TypeOf((*MockActor)(nil).CreateChild), varargs...)
}

// Handle mocks base method.
func (m *MockActor) Handle() actor.Handle {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Handle")
	ret0, _ := ret[0].(actor.Handle)
	return ret0
}

// Handle indicates an expected call of Handle.
func (mr *MockActorMockRecorder) Handle() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Handle", reflect.TypeOf((*MockActor)(nil).Handle))
}

// Invoke mocks base method.
func (m *MockActor) Invoke(msg actor.Envelope) (<-chan actor.Envelope, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Invoke", msg)
	ret0, _ := ret[0].(<-chan actor.Envelope)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Invoke indicates an expected call of Invoke.
func (mr *MockActorMockRecorder) Invoke(msg any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Invoke", reflect.TypeOf((*MockActor)(nil).Invoke), msg)
}

// Limiter mocks base method.
func (m *MockActor) Limiter() actor.RateLimiter {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Limiter")
	ret0, _ := ret[0].(actor.RateLimiter)
	return ret0
}

// Limiter indicates an expected call of Limiter.
func (mr *MockActorMockRecorder) Limiter() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Limiter", reflect.TypeOf((*MockActor)(nil).Limiter))
}

// Parent mocks base method.
func (m *MockActor) Parent() actor.Handle {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Parent")
	ret0, _ := ret[0].(actor.Handle)
	return ret0
}

// Parent indicates an expected call of Parent.
func (mr *MockActorMockRecorder) Parent() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Parent", reflect.TypeOf((*MockActor)(nil).Parent))
}

// Publish mocks base method.
func (m *MockActor) Publish(msg actor.Envelope) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Publish", msg)
	ret0, _ := ret[0].(error)
	return ret0
}

// Publish indicates an expected call of Publish.
func (mr *MockActorMockRecorder) Publish(msg any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Publish", reflect.TypeOf((*MockActor)(nil).Publish), msg)
}

// Receive mocks base method.
func (m *MockActor) Receive(msg actor.Envelope) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Receive", msg)
	ret0, _ := ret[0].(error)
	return ret0
}

// Receive indicates an expected call of Receive.
func (mr *MockActorMockRecorder) Receive(msg any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Receive", reflect.TypeOf((*MockActor)(nil).Receive), msg)
}

// RemoveBehavior mocks base method.
func (m *MockActor) RemoveBehavior(behavior string) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "RemoveBehavior", behavior)
}

// RemoveBehavior indicates an expected call of RemoveBehavior.
func (mr *MockActorMockRecorder) RemoveBehavior(behavior any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RemoveBehavior", reflect.TypeOf((*MockActor)(nil).RemoveBehavior), behavior)
}

// Security mocks base method.
func (m *MockActor) Security() actor.SecurityContext {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Security")
	ret0, _ := ret[0].(actor.SecurityContext)
	return ret0
}

// Security indicates an expected call of Security.
func (mr *MockActorMockRecorder) Security() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Security", reflect.TypeOf((*MockActor)(nil).Security))
}

// Send mocks base method.
func (m *MockActor) Send(msg actor.Envelope) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Send", msg)
	ret0, _ := ret[0].(error)
	return ret0
}

// Send indicates an expected call of Send.
func (mr *MockActorMockRecorder) Send(msg any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Send", reflect.TypeOf((*MockActor)(nil).Send), msg)
}

// Start mocks base method.
func (m *MockActor) Start() error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Start")
	ret0, _ := ret[0].(error)
	return ret0
}

// Start indicates an expected call of Start.
func (mr *MockActorMockRecorder) Start() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Start", reflect.TypeOf((*MockActor)(nil).Start))
}

// Stop mocks base method.
func (m *MockActor) Stop() error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Stop")
	ret0, _ := ret[0].(error)
	return ret0
}

// Stop indicates an expected call of Stop.
func (mr *MockActorMockRecorder) Stop() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Stop", reflect.TypeOf((*MockActor)(nil).Stop))
}

// Subscribe mocks base method.
func (m *MockActor) Subscribe(topic string, setup ...actor.BroadcastSetup) error {
	m.ctrl.T.Helper()
	varargs := []any{topic}
	for _, a := range setup {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "Subscribe", varargs...)
	ret0, _ := ret[0].(error)
	return ret0
}

// Subscribe indicates an expected call of Subscribe.
func (mr *MockActorMockRecorder) Subscribe(topic any, setup ...any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	varargs := append([]any{topic}, setup...)
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Subscribe", reflect.TypeOf((*MockActor)(nil).Subscribe), varargs...)
}

// Supervisor mocks base method.
func (m *MockActor) Supervisor() actor.Handle {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Supervisor")
	ret0, _ := ret[0].(actor.Handle)
	return ret0
}

// Supervisor indicates an expected call of Supervisor.
func (mr *MockActorMockRecorder) Supervisor() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Supervisor", reflect.TypeOf((*MockActor)(nil).Supervisor))
}

// MockSecurityContext is a mock of SecurityContext interface.
type MockSecurityContext struct {
	ctrl     *gomock.Controller
	recorder *MockSecurityContextMockRecorder
}

// MockSecurityContextMockRecorder is the mock recorder for MockSecurityContext.
type MockSecurityContextMockRecorder struct {
	mock *MockSecurityContext
}

// NewMockSecurityContext creates a new mock instance.
func NewMockSecurityContext(ctrl *gomock.Controller) *MockSecurityContext {
	mock := &MockSecurityContext{ctrl: ctrl}
	mock.recorder = &MockSecurityContextMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockSecurityContext) EXPECT() *MockSecurityContextMockRecorder {
	return m.recorder
}

// Capability mocks base method.
func (m *MockSecurityContext) Capability() ucan.CapabilityContext {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Capability")
	ret0, _ := ret[0].(ucan.CapabilityContext)
	return ret0
}

// Capability indicates an expected call of Capability.
func (mr *MockSecurityContextMockRecorder) Capability() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Capability", reflect.TypeOf((*MockSecurityContext)(nil).Capability))
}

// DID mocks base method.
func (m *MockSecurityContext) DID() actor.DID {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "DID")
	ret0, _ := ret[0].(actor.DID)
	return ret0
}

// DID indicates an expected call of DID.
func (mr *MockSecurityContextMockRecorder) DID() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "DID", reflect.TypeOf((*MockSecurityContext)(nil).DID))
}

// Discard mocks base method.
func (m *MockSecurityContext) Discard(msg actor.Envelope) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "Discard", msg)
}

// Discard indicates an expected call of Discard.
func (mr *MockSecurityContextMockRecorder) Discard(msg any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Discard", reflect.TypeOf((*MockSecurityContext)(nil).Discard), msg)
}

// Grant mocks base method.
func (m *MockSecurityContext) Grant(sub, aud did.DID, caps []ucan.Capability, expiry time.Duration) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Grant", sub, aud, caps, expiry)
	ret0, _ := ret[0].(error)
	return ret0
}

// Grant indicates an expected call of Grant.
func (mr *MockSecurityContextMockRecorder) Grant(sub, aud, caps, expiry any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Grant", reflect.TypeOf((*MockSecurityContext)(nil).Grant), sub, aud, caps, expiry)
}

// ID mocks base method.
func (m *MockSecurityContext) ID() actor.ID {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ID")
	ret0, _ := ret[0].(actor.ID)
	return ret0
}

// ID indicates an expected call of ID.
func (mr *MockSecurityContextMockRecorder) ID() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ID", reflect.TypeOf((*MockSecurityContext)(nil).ID))
}

// Nonce mocks base method.
func (m *MockSecurityContext) Nonce() uint64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Nonce")
	ret0, _ := ret[0].(uint64)
	return ret0
}

// Nonce indicates an expected call of Nonce.
func (mr *MockSecurityContextMockRecorder) Nonce() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Nonce", reflect.TypeOf((*MockSecurityContext)(nil).Nonce))
}

// PrivKey mocks base method.
func (m *MockSecurityContext) PrivKey() crypto.PrivKey {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "PrivKey")
	ret0, _ := ret[0].(crypto.PrivKey)
	return ret0
}

// PrivKey indicates an expected call of PrivKey.
func (mr *MockSecurityContextMockRecorder) PrivKey() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "PrivKey", reflect.TypeOf((*MockSecurityContext)(nil).PrivKey))
}

// Provide mocks base method.
func (m *MockSecurityContext) Provide(msg *actor.Envelope, invoke, delegate []actor.Capability) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Provide", msg, invoke, delegate)
	ret0, _ := ret[0].(error)
	return ret0
}

// Provide indicates an expected call of Provide.
func (mr *MockSecurityContextMockRecorder) Provide(msg, invoke, delegate any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Provide", reflect.TypeOf((*MockSecurityContext)(nil).Provide), msg, invoke, delegate)
}

// ProvideBroadcast mocks base method.
func (m *MockSecurityContext) ProvideBroadcast(msg *actor.Envelope, topic string, broadcast []actor.Capability) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ProvideBroadcast", msg, topic, broadcast)
	ret0, _ := ret[0].(error)
	return ret0
}

// ProvideBroadcast indicates an expected call of ProvideBroadcast.
func (mr *MockSecurityContextMockRecorder) ProvideBroadcast(msg, topic, broadcast any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ProvideBroadcast", reflect.TypeOf((*MockSecurityContext)(nil).ProvideBroadcast), msg, topic, broadcast)
}

// Require mocks base method.
func (m *MockSecurityContext) Require(msg actor.Envelope, invoke []actor.Capability) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Require", msg, invoke)
	ret0, _ := ret[0].(error)
	return ret0
}

// Require indicates an expected call of Require.
func (mr *MockSecurityContextMockRecorder) Require(msg, invoke any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Require", reflect.TypeOf((*MockSecurityContext)(nil).Require), msg, invoke)
}

// RequireBroadcast mocks base method.
func (m *MockSecurityContext) RequireBroadcast(msg actor.Envelope, topic string, broadcast []actor.Capability) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "RequireBroadcast", msg, topic, broadcast)
	ret0, _ := ret[0].(error)
	return ret0
}

// RequireBroadcast indicates an expected call of RequireBroadcast.
func (mr *MockSecurityContextMockRecorder) RequireBroadcast(msg, topic, broadcast any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RequireBroadcast", reflect.TypeOf((*MockSecurityContext)(nil).RequireBroadcast), msg, topic, broadcast)
}

// Sign mocks base method.
func (m *MockSecurityContext) Sign(msg *actor.Envelope) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Sign", msg)
	ret0, _ := ret[0].(error)
	return ret0
}

// Sign indicates an expected call of Sign.
func (mr *MockSecurityContextMockRecorder) Sign(msg any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Sign", reflect.TypeOf((*MockSecurityContext)(nil).Sign), msg)
}

// Verify mocks base method.
func (m *MockSecurityContext) Verify(msg actor.Envelope) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Verify", msg)
	ret0, _ := ret[0].(error)
	return ret0
}

// Verify indicates an expected call of Verify.
func (mr *MockSecurityContextMockRecorder) Verify(msg any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Verify", reflect.TypeOf((*MockSecurityContext)(nil).Verify), msg)
}

// MockRateLimiter is a mock of RateLimiter interface.
type MockRateLimiter struct {
	ctrl     *gomock.Controller
	recorder *MockRateLimiterMockRecorder
}

// MockRateLimiterMockRecorder is the mock recorder for MockRateLimiter.
type MockRateLimiterMockRecorder struct {
	mock *MockRateLimiter
}

// NewMockRateLimiter creates a new mock instance.
func NewMockRateLimiter(ctrl *gomock.Controller) *MockRateLimiter {
	mock := &MockRateLimiter{ctrl: ctrl}
	mock.recorder = &MockRateLimiterMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockRateLimiter) EXPECT() *MockRateLimiterMockRecorder {
	return m.recorder
}

// Acquire mocks base method.
func (m *MockRateLimiter) Acquire(msg actor.Envelope) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Acquire", msg)
	ret0, _ := ret[0].(error)
	return ret0
}

// Acquire indicates an expected call of Acquire.
func (mr *MockRateLimiterMockRecorder) Acquire(msg any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Acquire", reflect.TypeOf((*MockRateLimiter)(nil).Acquire), msg)
}

// Allow mocks base method.
func (m *MockRateLimiter) Allow(msg actor.Envelope) bool {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Allow", msg)
	ret0, _ := ret[0].(bool)
	return ret0
}

// Allow indicates an expected call of Allow.
func (mr *MockRateLimiterMockRecorder) Allow(msg any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Allow", reflect.TypeOf((*MockRateLimiter)(nil).Allow), msg)
}

// Config mocks base method.
func (m *MockRateLimiter) Config() actor.RateLimiterConfig {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Config")
	ret0, _ := ret[0].(actor.RateLimiterConfig)
	return ret0
}

// Config indicates an expected call of Config.
func (mr *MockRateLimiterMockRecorder) Config() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Config", reflect.TypeOf((*MockRateLimiter)(nil).Config))
}

// Release mocks base method.
func (m *MockRateLimiter) Release(msg actor.Envelope) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "Release", msg)
}

// Release indicates an expected call of Release.
func (mr *MockRateLimiterMockRecorder) Release(msg any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Release", reflect.TypeOf((*MockRateLimiter)(nil).Release), msg)
}

// SetConfig mocks base method.
func (m *MockRateLimiter) SetConfig(cfg actor.RateLimiterConfig) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "SetConfig", cfg)
}

// SetConfig indicates an expected call of SetConfig.
func (mr *MockRateLimiterMockRecorder) SetConfig(cfg any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SetConfig", reflect.TypeOf((*MockRateLimiter)(nil).SetConfig), cfg)
}
