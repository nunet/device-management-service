| | |
| --- | --- |
| **Feature name** | _Title summarizing the functionality you want to build_ |
| **Overview / summary** | _a) Work package; <br>b) Description of what the feature will entail — you can also include any background information that will help the team; Is this a use-oriented feature or a capability (as per SAFe definition)?_ | 
| **Timing** | _When you plan to ship the new customer experience; Obviously this is related to the milestone for which we are freezing the scope; However, the features may have dependencies / relations that need to be taken into account;_ |
| **Status** | _a) Not started / on track / at risk <br>b) short explanation_ |
| **Team** | _Product manager, development team, designers, QA, etc._ |
| **Strategic alignment** | _Explanation of how this supports business and product goals — why are you building this feature now?_ |
| **Who it benefits** | _Who will benefit from the feature — link to any personas you have_ |
| **User challenge** | _The problem the user is trying to solve and ways they may be attempting to solve it_ |
| **Value score** | _The value estimate for the feature (feature / capability score)_ |
| **Design** | _ a) Query to related issues on gitlab; <br>b) Relations to related design discussions / decisions; _ |
| **Impacted functionality** | _Take note of any other functionality that this feature may affect (features in this milestone or further)_ |
| **Open questions** | _Important aspects that still have to be solved in order to fully include the defined scope of the issue into the release_ |
| **Notes** | _Additional background information and anything else that the team may need quick access to_ |