// Copyright 2024, Nunet
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.

package backgroundtasks

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestTaskExecution(t *testing.T) {
	task := Task{
		Name:        "Test Task",
		Description: "A task for testing",
		Function: func(_ interface{}) error {
			// Simple test function that does nothing
			return nil
		},
		RetryPolicy: RetryPolicy{
			MaxRetries: 1,
			Delay:      1 * time.Second,
		},
	}

	err := task.Function(nil)
	assert.NoError(t, err, "Task function should execute without error")
}
