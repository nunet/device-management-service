// Copyright 2024, Nunet
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.

package config

type Config struct {
	Profiler      `mapstructure:"profiler" json:"profiler"`
	General       `mapstructure:"general" json:"general"`
	Rest          `mapstructure:"rest" json:"rest"`
	P2P           `mapstructure:"p2p" json:"p2p"`
	Job           `mapstructure:"job" json:"job"`
	Observability `mapstructure:"observability" json:"observability"`
	APM           `mapstructure:"apm" json:"apm"`
}

type General struct {
	UserDir                string `mapstructure:"user_dir" json:"user_dir"`
	WorkDir                string `mapstructure:"work_dir" json:"work_dir"`
	DataDir                string `mapstructure:"data_dir" json:"data_dir"`
	Debug                  bool   `mapstructure:"debug" json:"debug"`
	HostCity               string `mapstructure:"host_city" json:"host_city"`
	HostCountry            string `mapstructure:"host_country" json:"host_country"`
	HostContinent          string `mapstructure:"host_continent" json:"host_continent"`
	PortAvailableRangeFrom int    `mapstructure:"port_available_range_from" json:"port_available_range_from"`
	PortAvailableRangeTo   int    `mapstructure:"port_available_range_to" json:"port_available_range_to"`
}

type Rest struct {
	Addr string `mapstructure:"addr" json:"addr"`
	Port uint32 `mapstructure:"port" json:"port"`
}

type Profiler struct {
	Enabled bool   `mapstructure:"enabled" json:"enabled"`
	Addr    string `mapstructure:"addr" json:"addr"`
	Port    uint32 `mapstructure:"port" json:"port"`
}

type P2P struct {
	ListenAddress   []string `mapstructure:"listen_address" json:"listen_address"`
	BootstrapPeers  []string `mapstructure:"bootstrap_peers" json:"bootstrap_peers"`
	Memory          int      `mapstructure:"memory" json:"memory"`
	FileDescriptors int      `mapstructure:"fd" json:"fd"`
}

type Job struct {
	AllowPrivilegedDocker bool `mapstructure:"allow_privileged_docker" json:"allow_privileged_docker"`
}

type Observability struct {
	LogLevel             string `mapstructure:"log_level" json:"log_level"`
	LogFile              string `mapstructure:"log_file" json:"log_file"`
	MaxSize              int    `mapstructure:"max_size" json:"max_size"` // in megabytes
	MaxBackups           int    `mapstructure:"max_backups" json:"max_backups"`
	MaxAge               int    `mapstructure:"max_age" json:"max_age"` // in days
	ElasticsearchURL     string `mapstructure:"elasticsearch_url" json:"elasticsearch_url"`
	ElasticsearchIndex   string `mapstructure:"elasticsearch_index" json:"elasticsearch_index"`
	FlushInterval        int    `mapstructure:"flush_interval" json:"flush_interval"`               // in seconds
	ElasticsearchEnabled bool   `mapstructure:"elasticsearch_enabled" json:"elasticsearch_enabled"` // disable elastic logging
	ElasticsearchAPIKey  string `mapstructure:"elasticsearch_api_key" json:"elasticsearch_api_key"`
	InsecureSkipVerify   bool   `mapstructure:"insecure_skip_verify" json:"insecure_skip_verify"` // allow insecure TLS connections
}

type APM struct {
	ServerURL   string `mapstructure:"server_url" json:"server_url"`
	ServiceName string `mapstructure:"service_name" json:"service_name"`
	Environment string `mapstructure:"environment" json:"environment"`
	APIKey      string `mapstructure:"api_key" json:"api_key"`
}
