## Estimation

Estimated focus duration (perfect conditions): X days <br>
Estimated pessimistic duration (worst case scenario): X days <br>

## Description

### Who
 
1. 

### What

1. 

### How

1. 

### Why

1. 

### When

1. 


<br>


## Acceptance Criteria

1. 

</details>

## Work Breakdown Structure (WBS)

| Task |  Description  | Duration  | Status | Start Date | End Date | Comment |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| A |  | x Hrs | Done/In Progress |  |  |  |

